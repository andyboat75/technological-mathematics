# <div align="center"> Technological Mathematics </div>

This page is a collection of tools, techniques and theories required to solve on a computer the mathematical models of problems in science and engineering." Topics to be considered are mainly based on numerical simulation of mathematical models and domain data/information.