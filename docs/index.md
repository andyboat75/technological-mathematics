# <div align=center style="color:black">Mathematics in Technology</div>
<hr style="width:100%; height:2px; text-align:center; background-color:black;">
<p>
This page is a collection of tools, techniques and theories required to solve on a computer the mathematical models of problems in science and engineering." Topics to be considered are mainly based on numerical simulation of mathematical models and domains.
</p>
<div>
The objective behind a simulation depends on the domain
of the application under simulation. The objective can be to understand the cause behind an event,
reconstruct a specific situation, optimize the process, or predict the occurrence of an event. 
There are several situations where numerical simulation is the only choice, or the best choice. 
There are some phenomena or situations where performing experiments is almost impossible, for example,
climate research, astrophysics, and weather forecasts. In some other situations, actual experiments are
not preferable, for example, to check the stability or strength of some material or product. Some 
experiments are very costly in terms of time/economy, such as car crashes or life science 
experiments. In such scenarios, scientific computing helps users analyze and solve problems 
without spending much time or cost.
</div>

## Applications
<div class="portfolio_container">
    <div class="portfolio_card card-left">
        <a href="finite_element_multi_physics/">
            <img class="portfolio_img" src="finite_element_multi_physics/images/portfolio_image.jpg">
            <h3 class="card-title">Finite Element Method for Multi-Physics</h3>
        </a>
    </div>
<!--- flex card--->
    <div class="portfolio_card card-center">
    <a href="numerics_fluid_mechanics/">
    <img class="portfolio_img" src="numerics_fluid_mechanics/images/portfolio_image.jpg">
    <h3 class="card-title">Numerical Methods of fluid mechanics</h3>
    </a>
    </div>
<!--- flex card--->
    <div class="portfolio_card card-right ">
        <a href="pde_modelling/">
        <img class="portfolio_img" src="pde_modelling/images/pde_modelling_portfolio_images.jpeg">
        <h3 class="card-title">PDE Modelling</h3>
        </a>
    </div>

</div>

<!---
<div style="width: 100%; text-align: center">
<div style="width: 30%;"></div>
<div style="width: 30%;"></div>
<div style="width: 30%;"></div>
</div>
--->